//Получение элементов со страницы
const formSearch = document.querySelector('.form-search'),
    inputCitiesFrom = formSearch.querySelector('.input__cities-from'),
    dropdownCitiesFrom = formSearch.querySelector('.dropdown__cities-from'),
    inputCitiesTo = formSearch.querySelector('.input__cities-to'),
    dropdownCitiesTo = formSearch.querySelector('.dropdown__cities-to'),
    inputDateDepart = formSearch.querySelector('.input__date-depart'),
    cheapestTicket = document.getElementById('cheapest-ticket'),
    otherCheapTickets = document.getElementById('other-cheap-tickets');

//данные
const citiesAPI = './data/cities.json',
    codesAPI = './data/codesCountres.json';

//API aviasales и proxy
// const citiesAPI = 'http://api.travelpayouts.com/data/ru/cities.json',
const proxy = 'https://cors-anywhere.herokuapp.com/',
    API_KEY = 'cb22d7f57f22f5e9281cd0a563630050',
    calendar = 'https://min-prices.aviasales.ru/calendar_preload';

let cities = [],
    citiesCodes = {};

    
// Функции
const getData = (url, callback) => {
    const request = new XMLHttpRequest();
    request.open('GET', url);

    request.addEventListener('readystatechange', () => {
        if (request.readyState !== 4) return;
        if(request.status === 200){
            callback(request.response);
        } else {
            console.error(request.status);
        }
    });

    request.send();
};

const showCities = (input, list) => {
    list.textContent = '';

    const inputValue = input.value.trim();

    if(inputValue === '') return;

    const filterCities = cities.filter((city)=>{
        return city.name.toLowerCase().trim().includes(inputValue.toLowerCase());
    });

    const elements = [];

    const compare = (a, b) => a.name > b.name ? 1 : -1;
    //Сортировка городов, которые начинаются с введенной подстроки, по алфавиту
    const includesInput = filterCities.filter(city => city.name.slice(0, inputValue.length).toLowerCase() === inputValue.toLowerCase()).sort(compare);
    //Сортировка городов, которые включают искомую подстроку, по алфавиту
    const excludesInput = filterCities.filter(city => city.name.slice(0, inputValue.length).toLowerCase() !== inputValue.toLowerCase()).sort(compare);

    //Вывод городов в подсказке
    [...includesInput, ...excludesInput].forEach((city)=>{
        const li = document.createElement('li');
        li.classList.add('dropdown__city');
        li.textContent = city.name.split('(')[0]+ ", " + citiesCodes[city.country_code].split("(")[0];
        elements.push(li);
    })
    //Если остался один искомый элемент, заполнение им инпута
    if(elements.length === 1) {
        list.textContent = '';
        input.value = elements[0].textContent
    } else {
        list.append(...elements)
    }
};

const selectCity = (e, input, list)=>{
    if(e.target.tagName.toLowerCase() === 'li') {
        input.value = e.target.textContent
        list.textContent = '';
    }   
};

const getChanges = num => {
    if (num){
        return num === 1 ? "С одной пересадкой" : "С двумя пересадками"
    } else {
        return "Без пересадок"
    }
}

const getDate = date => {
    return new Date(date).toLocaleString('ru', {
        year: 'numeric',
        month: 'long',
        day: 'numeric'
    });
}

const getCityName = cityCode => {
    return cities.find(item => item.code === cityCode).name
}

const getlinkAviasales = (el) => {
    const date = el.depart_date.split('-')
    return `https://www.aviasales.ru/search/${el.origin}${date[2]}${date[1]}${el.destination}1`
}

const createTicketCard = el => {
    const ticket = document.createElement('article');
    ticket.classList.add('ticket');
    let deep = '';
    if (el) {
        deep = `
            <h3 class="agent">${el.gate}</h3>
            <div class="ticket__wrapper">
                <div class="left-side">
                    <a href="${getlinkAviasales(el)}" target="_blank" class="button button__buy">Купить
                        за ${el.value}₽</a>
                </div>
                <div class="right-side">
                    <div class="block-left">
                        <div class="city__from">Вылет из города
                            <span class="city__name">${getCityName(el.origin)}</span>
                        </div>
                        <div class="date">${getDate(el.depart_date)}</div>
                    </div>
            
                    <div class="block-right">
                        <div class="changes">${getChanges(el.number_of_changes)}</div>
                        <div class="city__to">Город назначения:
                            <span class="city__name">${getCityName(el.destination)}</span>
                        </div>
                    </div>
                </div>
            </div>
        `;
    } else {
        deep = "<h3>К сожалению, на текущую дату билетов не нашлось!</h3>"
    }



    ticket.insertAdjacentHTML('afterbegin', deep);
    return ticket;
}

const renderTicketsAll = tickets => {
    otherCheapTickets.style.display = 'block';
    otherCheapTickets.innerHTML = '<h2>Самые дешевые билеты на другие даты</h2>';
    let ticketElems = []; 
    
    tickets.slice(0, 10).forEach(el => {
        ticketElems.push(createTicketCard(el))
    });
    console.log(ticketElems);
    otherCheapTickets.append(...ticketElems);

}

const renderTicketsDay = tickets => {
    cheapestTicket.style.display = 'block';
    cheapestTicket.innerHTML = '<h2>Самый дешевый билет на выбранную дату</h2>';
    const ticketElem = createTicketCard(tickets[0]);
    cheapestTicket.append(ticketElem);
}

const renderCheap = (data, depart_date) => {
    const tickets = JSON.parse(data).best_prices;
    
    const ticketsDay = tickets.filter(item=> item.depart_date === depart_date);
    //Функция для сортировки по дате
    const compire = (a, b) => new Date(a.depart_date) > new Date(b.depart_date) ? 1 : -1;

    renderTicketsAll(tickets.sort(compire));
    renderTicketsDay(ticketsDay.sort(compire));
}


//Обработчики событий
inputCitiesFrom.addEventListener('input', ()=>{
    inputCitiesFrom.style.border = 'none';
    inputCitiesFrom.parentElement.parentElement.classList.remove('error');

    showCities(inputCitiesFrom, dropdownCitiesFrom);
});

inputCitiesTo.addEventListener('input', ()=>{
    inputCitiesTo.style.border = 'none';
    inputCitiesTo.parentElement.parentElement.classList.remove('error');

    showCities(inputCitiesTo, dropdownCitiesTo);
});

dropdownCitiesFrom.addEventListener('click', ()=>{selectCity(event, inputCitiesFrom, dropdownCitiesFrom)});

dropdownCitiesTo.addEventListener('click', ()=>{selectCity(event,inputCitiesTo, dropdownCitiesTo)});

formSearch.addEventListener('submit', (e)=>{
    e.preventDefault();

    const findCityCode = input => {
        const cityName = input.value.split(',')[0]
        const city = cities.find(city=> cityName === city.name);
        return city ? city.code : null
    }

    const origin = findCityCode(inputCitiesFrom);
    const destination = findCityCode(inputCitiesTo);

    if(origin && destination){
        const formData = {
            origin,
            destination,
            depart_date: inputDateDepart.value,
        };
    
        const requestData = `?depart_date=${formData.depart_date}&origin=${formData.origin}&destination=${formData.destination}&one_way=true`;
        
        getData(calendar + requestData, response => {
            renderCheap(response, formData.depart_date);
        });
    } else {
        if(!origin){
            inputCitiesFrom.style.border = '1px solid red';
            inputCitiesFrom.parentElement.parentElement.classList.add('error');
        }
        if(!destination){
            inputCitiesTo.style.border = '1px solid red';
            inputCitiesTo.parentElement.parentElement.classList.add('error');
        } 
    }
});

//вызовы функций
getData(citiesAPI, data => {
    cities = JSON.parse(data).filter(city => city.name);
});
getData(codesAPI, data => {
    citiesCodes = JSON.parse(data)[0];
});


